from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('readimage/', views.read_image, name='read_image'),
]
