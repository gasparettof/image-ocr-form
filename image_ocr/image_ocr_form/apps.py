from django.apps import AppConfig


class ImageOcrFormConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'image_ocr_form'
