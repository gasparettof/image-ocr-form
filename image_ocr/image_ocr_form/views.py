import torch
import torch.nn as nn
from PIL import Image
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
from torchvision import transforms


class CustomNeuralNetwork(nn.Module):
    def __init__(self):
        super(CustomNeuralNetwork, self).__init__()
        self.fc1 = nn.Linear(28 * 28, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 10)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x


def home(request):
    return render(request, 'image_ocr_form/home.html')


def transform_image(image):
    # Converte l'immagine in formato RGBA
    image = image.convert('RGBA')

    white_image = Image.new('RGB', image.size, 'white')
    white_image.paste(image, (0, 0), image)

    grayscale_image = white_image.convert('L')

    inverted_image = Image.eval(grayscale_image, lambda x: 255 - x)
    resized_image = inverted_image.resize((829, 825))
    resized_image.info['dpi'] = (56, 56)
    return resized_image


def read_image(request):
    if request.method == 'POST':
        if 'imageFile' in request.FILES:
            image_data = request.FILES['imageFile']
            image = Image.open(image_data)

            image = transform_image(image)

            # just to check the image
            image.save('test.jpg', dpi=(56, 56))

            # Definisci il percorso del file del modello PyTorch
            model_path = settings.TORCH_MODEL_PATH

            # Crea un'istanza del modello
            model = CustomNeuralNetwork()
            model.load_state_dict(torch.load(model_path, map_location='cpu'))
            model.eval()  # Imposta il modello in modalità valutazione

            transform = transforms.Compose([
                transforms.Grayscale(),
                transforms.Resize((28, 28)),
                transforms.ToTensor(),
            ])

            # Applica la trasformazione all'immagine per adattarla al modello
            image_tensor = transform(image).unsqueeze(0)  # Applica la trasformazione e aggiungi una dimensione batch

            with torch.no_grad():
                output = model(image_tensor)
                predicted_number = torch.argmax(output, dim=1).item()

            return JsonResponse({'predicted_number': predicted_number})
        else:
            return JsonResponse({'error': 'Nessun file di immagine ricevuto'}, status=400)

    else:
        return JsonResponse({'error': 'Metodo non consentito'}, status=405)
