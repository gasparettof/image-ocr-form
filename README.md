# Draw-and-Predict Web Application

This web application combines a drawing canvas with a pre-trained neural network for handwritten digit recognition. Users can draw on the canvas, and the system will predict the drawn number using the neural network model.

## Usage

1. Start by drawing on the canvas provided using your mouse or touch input.
2. Once you finish drawing, give the system a moment to process the image.
3. The drawn image will be sent to the server for prediction using a neural network model previously trained on the MNIST dataset.
4. After prediction, the result will be displayed on the image for user feedback.
5. Explore different drawing possibilities and witness the neural network capabilities in action!

## Technologies Used

- Fabric.js: JavaScript library for interactive canvas drawing.
- Python Django: Backend framework for image processing and predictions.
- Pretrained Neural Network: Utilized for image recognition and prediction tasks.

The application seamlessly integrates drawing input with neural network prediction, providing an interactive experience for users interested in automated digit recognition.

## Handwritten Digit Recognition Neural Network

This project encompasses a PyTorch-based neural network solution for recognizing handwritten digits from the MNIST dataset. Here are some key points from the neural network project:

- Utilizes the MNIST dataset containing images of digits 0 to 9.
- Trains a neural network model for accurate digit classification.
- Evaluates the model performance on a test dataset for validation.
- Dependencies include `torch` and `torchvision` libraries, installable via pip.
- MNIST dataset consists of 28x28 grayscale images for training and testing.
